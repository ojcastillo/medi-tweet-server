import json
import logging
import os
from datetime import datetime

from flask import Flask, jsonify, render_template, request
from flask.logging import default_handler
from pythonjsonlogger import jsonlogger

from medi_tweet.db.model import db
from medi_tweet.services.cache_service import CacheService
from medi_tweet.services.db_service import DBService
from medi_tweet.services.dms_service import DMsService
from medi_tweet.services.exceptions.service_exception import ServiceException
from medi_tweet.services.ingestor_service import TweetIngestorService
from medi_tweet.services.instructions_service import InstructionTweetsService
from medi_tweet.services.meditweet_service import MediTweetsService
from medi_tweet.services.permalink_service import PermalinkService

logger = logging.getLogger()

if os.environ.get("DEBUG_MODE", False):
    formatter = jsonlogger.JsonFormatter("%(asctime) %(levelname) %(name) %(module) %(lineno) %(message)")
else:
    formatter = jsonlogger.JsonFormatter("%(asctime) %(levelname) %(module) %(message)")

default_handler.setFormatter(formatter)
logger.setLevel(os.environ.get("LOGLEVEL", "INFO").upper())
logger.addHandler(default_handler)

app = Flask(__name__, template_folder="templates")
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ["SQLALCHEMY_MYSQL_URI"]
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db.init_app(app)

db_service = DBService()
cache_service = CacheService(default_timeout=6 * 60)
permalink_service = PermalinkService(db_service, cache_service)

BLACKOUTS_HASHTAGS = [s.strip() for s in os.environ.get("BLACKOUTS_HASHTAGS", "SinLuz").split(",")]
BLACKOUTS_RAW_TWEET_TOPIC = os.environ["BLACKOUTS_RAW_TWEET_TOPIC"]

MEDITWEET_HASHTAGS = [s.strip() for s in os.environ.get("MEDITWEET_HASHTAGS", "MediTweet,ServicioPublico").split(",")]
MEDITWEET_RAW_TWEET_TOPIC = os.environ["MEDITWEET_RAW_TWEET_TOPIC"]
MEDITWEET_RESPONSES_TOPIC = os.environ["MEDITWEET_RESPONSES_TOPIC"]
MEDITWEET_CLASSIFIED_TWEET_TOPIC = os.environ["MEDITWEET_CLASSIFIED_TWEET_TOPIC"]

PUBSUB_PROJECT_ID = os.environ["PUBSUB_PROJECT_ID"]

blackouts_tweet_service = TweetIngestorService(
    BLACKOUTS_HASHTAGS, db_service, 2, PUBSUB_PROJECT_ID, BLACKOUTS_RAW_TWEET_TOPIC
)


topic_paths = {
    "classified_tweets": MEDITWEET_CLASSIFIED_TWEET_TOPIC,
    "raw_tweets": MEDITWEET_RAW_TWEET_TOPIC,
    "tweet_response": MEDITWEET_RESPONSES_TOPIC,
}
meditweets_service = MediTweetsService(MEDITWEET_HASHTAGS, db_service, 1, PUBSUB_PROJECT_ID, topic_paths)

instructions_service = InstructionTweetsService()
dms_service = DMsService(db_service, 3)


@app.route("/api/ingest")
def ingestor(words=[]):
    try:
        blackout_response = blackouts_tweet_service.ingest()
        meditweet_response = meditweets_service.ingest_tweets()
    except ServiceException as e:
        logger.error("Unable to ingest Tweets, {}".format(e))

        return e, 500

    return (f"Ingested {len(blackout_response)} tweets for Blackouts and {len(meditweet_response)} for MediTweets", 200)


@app.route("/api/tweet_instructions")
def tweet_instructions():
    try:
        instructions_service.tweet_instructions()
    except Exception as e:
        logger.error("Unable to send instruction tweet, {}".format(e))

        return e, 500

    return "Instruction Tweet sent"


@app.route("/api/process_dms")
def process_dms():
    try:
        dms_service.process_dms()
    except Exception as e:
        logger.error("Unable to process DMs, {}".format(e))

        return e, 500

    return "Processed DMs, generated replies!"


@app.route("/api/predictor", methods=["POST"])
def predictor():
    req = request.json["tweets"]
    logger.debug(f"Predict Request: {req}")
    result = meditweets_service.prediction_service.predict(req)

    return jsonify(result)


@app.route("/test/response")
def tweet_all_responses():
    meditweets_service.tweet_all_responses()

    # TODO: Better return messages

    return "200", 200


@app.route("/api/ingredient/<component>")
def get_ingredient(component):
    return json.dumps(db_service.get_tweets(component, as_json=True))


@app.route("/p/<ingredient>")
def permalink(ingredient):
    data = permalink_service.permalink(ingredient)

    if data:
        return render_template("index.html", ingredient=ingredient, data=data)
    else:
        return server_error("No Data found")


@app.route("/")
def landing():
    return render_template("landing.html")


@app.errorhandler(500)
def server_error(e):
    logger.exception("An error occurred during a request. Stacktrace: {}".format(e))

    return (
        """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(
            e.__class__
        ),
        500,
    )


@app.route("/health", methods=["GET"])
def ping():
    return jsonify({"tms": datetime.now().timestamp()})


def setup_db():
    app.app_context().push()
    db.init_app(app)
    db.create_all()
    db.session.commit()


setup_db()

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)
