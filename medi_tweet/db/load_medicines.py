import csv
import os

from flask import Flask
from medi_tweet.db.model import db, Medicine

# TODO import from main module instead.
app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ["SQLALCHEMY_MYSQL_URI"]
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.app_context().push()
db.init_app(app)

with open("medicines.csv") as csv_file:
    csv_reader = csv.reader(csv_file)
    next(csv_reader)  # skip header
    for row in csv_reader:
        components_str = row[0].lower()
        components = components_str.split("-")
        name = row[1].lower()
        presentation = row[2].lower()
        code = row[3]
        medicine = Medicine(name=name, components=components, presentation=presentation, code=code)
        db.session.add(medicine)
    db.session.commit()
