import tweepy
import logging
import os


logger = logging.getLogger(__name__)


def tweepy_auth():
    access_secret = os.getenv("TWITTER_ACCESS_SECRET")
    access_token = os.getenv("TWITTER_ACCESS_TOKEN")
    consumer_key = os.getenv("TWITTER_CONSUMER_KEY")
    consumer_secret = os.getenv("TWITTER_CONSUMER_SECRET")

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_secret)
    api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
    try:
        api.verify_credentials()
    except Exception as e:
        logger.error("Error creating Twitter API", exc_info=True)
        raise e
    logger.info("Twitter API created")

    return api
