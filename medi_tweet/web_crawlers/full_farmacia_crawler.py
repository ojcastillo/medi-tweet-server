import requests
import re
from bs4 import BeautifulSoup
from medi_tweet.web_crawlers.base_crawler import BaseCrawler


class FullFarmaciaCrawler(BaseCrawler):
    def __init__(self, base_url):
        super().__init__(base_url)

    def crawl(self, medicine):
        try:
            self.logger.debug(f"Crawling for info about medicine:{medicine}")
            url = f"{self.url}{medicine}&go.x=0&go.y=0"
            source_code = requests.get(url, timeout=30)
        except Exception as e:
            self.logger.error(e)

            return []

        plain_text = source_code.text
        soup = BeautifulSoup(plain_text, "html.parser")
        table_of_products = soup.find(id="productContainer")
        tds = table_of_products.findAll("td")
        info = self._collect_info(tds)

        return info

    def _collect_info(self, tds):
        # Struct store: {'farmacia': 'FullFarmacia', 'Colinas de Bello Monte' : nombre, 'productos' : products}
        # Struct products: {'producto' : nombre, 'disponible' : disponibilidad}

        i = 6
        total_list = []
        products_by_store = []

        while i < len(tds):
            if tds[i].get("class")[0] == "productNameStyle":
                product_regex = re.split(r">|<", str(tds[i]))[10].strip()

                i += 3

                product = {"name": product_regex}
                products_by_store.append(product)

                i += 2

        if len(products_by_store) > 0:
            store = {"name": "FullFarmacia", "address": "Colinas de Bello Monte", "medicines": products_by_store}
            total_list.append(store)

        return total_list
