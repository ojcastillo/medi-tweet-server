import requests
import re
from bs4 import BeautifulSoup

from medi_tweet.web_crawlers.base_crawler import BaseCrawler


class FarmarketCrawler(BaseCrawler):
    def __init__(self, base_url):
        super().__init__(base_url)

    def crawl(self, medicine):
        try:
            self.logger.debug(f"Crawling for info about medicine:{medicine}")
            source_code = requests.post(self.url, data={"txtProducto": medicine}, timeout=30)
        except Exception as e:
            self.logger.error(e)

            return []

        plain_text = source_code.text
        soup = BeautifulSoup(plain_text, "html.parser")
        tds = soup.find_all("td")
        info = self._collect_info(tds)

        return info

    def _collect_info(self, tds):
        # Struct store: {'farmacia': 'Farmarket', 'sede' : nombre, 'productos' : productosPorTienda}
        # Struct products: {'producto' : nombre, 'disponibles' : xxx}

        i = 2
        total_list = []
        products_by_store = []

        while i < len(tds):

            if tds[i].get("class")[0] == "CeldaCentradaTextoBlanco" and tds[i].get("colspan") == "2":
                store_location = re.split(r":|[|]", tds[i].string)[1].strip()

                i += 4

                while i < len(tds) and tds[i].get("class")[0] == "Resultado":
                    product_regex = re.split(r">|<", str(tds[i]))[2]
                    product_regex = product_regex.split(" - Componente:")[0]
                    product = {"name": product_regex}
                    products_by_store.append(product)

                    i += 2

                if products_by_store:
                    store = {"name": "Farmarket", "address": store_location, "medicines": products_by_store}

                    total_list.append(store)

                products_by_store = []

        return total_list
