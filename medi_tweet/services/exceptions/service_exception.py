class ServiceException(Exception):
    def __init__(self, service, message=None, data=None):
        if message is None:
            message = f"An Exception occured in {service}"
        super(ServiceException, self).__init__(message)
        self.service = service
        self.data = data
