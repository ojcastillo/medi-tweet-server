import logging
import os


class BaseService:
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.debug = os.environ.get("DEBUG_MODE", False)

    def _init_mode(self):
        if self.debug:
            self.logger.info("Initialized in DEBUG MODE")
