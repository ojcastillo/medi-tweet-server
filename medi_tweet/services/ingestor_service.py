import json

from medi_tweet.services.base_service import BaseService
from medi_tweet.services.bus_service import GooglePublisherService
from medi_tweet.services.search_service import SearchService


class TweetIngestorService(BaseService):
    def __init__(self, hashtags, db_service, paginator_id, project_id, topic_path):
        super().__init__()
        self.db_service = db_service
        self.project_id = project_id
        self.bus_service = GooglePublisherService(self.project_id)
        self.paginator_id = paginator_id
        self.search_service = SearchService()
        self.hashtags = hashtags + [h.lower() for h in hashtags]
        self.raw_tweet_topic_path = topic_path
        self.logger.info(
            f"Service initialized with hashtags: {self.hashtags}, PubSub Project: {self.project_id}\
            and Topic: {self.raw_tweet_topic_path}"
        )

    def ingest(self):
        # When changing the search query, we should bump the paginator id.
        since_id = self.db_service.get_tweet_paginator_by_id(self.paginator_id)
        raw_tweets, max_id = self.search_service.search(words=[], hashtags=self.hashtags, since_id=since_id)
        self.db_service.update_tweet_paginator_by_id(self.paginator_id, max_id)

        for tweet in raw_tweets:
            self._publish_raw_tweet(tweet)

        return raw_tweets

    def _publish_raw_tweet(self, tweet):
        data = json.dumps(tweet._json).encode("utf-8")
        self.bus_service.publish_event(self.raw_tweet_topic_path, data=data)
