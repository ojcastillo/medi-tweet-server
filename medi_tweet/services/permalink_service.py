import json

from medi_tweet.services.base_service import BaseService
from medi_tweet.web_crawlers.full_farmacia_crawler import FullFarmaciaCrawler
from medi_tweet.web_crawlers.farmarket_crawler import FarmarketCrawler
from medi_tweet.web_crawlers.funda_farmacia_crawler import FundafarmaciaCrawler


class PermalinkService(BaseService):
    def __init__(self, db_service, cache_service):
        super().__init__()
        self.db_service = db_service
        self.cache_service = cache_service
        self.full_farmacia_crawler = FullFarmaciaCrawler(
            "http://fullfarmacia.com/catalog.do?page=1&offSet=0&op=requestSearch&searchBox="
        )
        self.farmarket_crawler = FarmarketCrawler(
            "https://www.farmarket.com.ve/sitio/index.php/resultados-busqueda-productos/"
        )
        self.funda_farmacia_crawler = FundafarmaciaCrawler("http://www.fundafarmacia.com/consulta/busqueda.php")

    def permalink(self, ingredient):
        self.logger.info("Request received: " + ingredient)

        # Query database
        data = self.db_service.get_tweets(ingredient)

        # Query Farmarket
        farmarket = self.cache_service.get_value_as_json("farmarket-" + ingredient)

        if not farmarket:
            farmarket = self.farmarket_crawler.crawl(ingredient)
            self.cache_service.set_value("farmarket-" + ingredient, json.dumps(farmarket), timeout=86400)

        # Query FullFarmacia
        full_farmacia = self.cache_service.get_value_as_json("fullFarmacia-" + ingredient)

        if not full_farmacia:
            full_farmacia = self.full_farmacia_crawler.crawl(ingredient)
            self.cache_service.set_value("fullFarmacia-" + ingredient, json.dumps(full_farmacia), timeout=86400)

        # Query FundaFarmacia
        funda_farmacia = self.cache_service.get_value_as_json("fundaFarmacia-" + ingredient)

        if not funda_farmacia:
            funda_farmacia = self.funda_farmacia_crawler.crawl(ingredient)
            self.cache_service.set_value("fundaFarmacia-" + ingredient, json.dumps(funda_farmacia), timeout=86400)

        # Merge the data
        data["pharmacies"] = {}

        if farmarket:
            data["pharmacies"]["farmarket"] = farmarket

        if funda_farmacia:
            data["pharmacies"]["fundaFarmacia"] = funda_farmacia

        if full_farmacia:
            data["pharmacies"]["fullFarmacia"] = full_farmacia

        return data
