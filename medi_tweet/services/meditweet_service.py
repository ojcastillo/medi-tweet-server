import json
import os
import time
import urllib

from pyshorteners import Shortener, Shorteners
from tweepy.error import TweepError

from medi_tweet.db import model
from medi_tweet.services.ingestor_service import TweetIngestorService
from medi_tweet.services.prediction_service import PredictionService
from medi_tweet.twitter.api import tweepy_auth

TWITTER_URL = "https://twitter.com/i/web/status/{}"
BETA_TESTERS = None


class MediTweetsService(TweetIngestorService):
    def __init__(self, hashtags, db_service, paginator_id, project_id, topic_paths, should_tweet=False):
        super().__init__(hashtags, db_service, paginator_id, project_id, topic_paths["raw_tweets"])
        self.sleep_time = float(os.environ.get("SLEEP_TIME", 10))
        self.permalink = "https://meditw.it/p/"
        self.should_tweet = should_tweet
        self.topic_paths = topic_paths

        model_file = os.environ["MEDITWEET_CLASSIFIER_MODEL_FILE"]
        vocabulary_file = os.environ["MEDITWEET_CLASSIFIER_VOCABULARY_FILE"]
        medicines_csv = os.environ.get("MEDITWEET_MEDICINES_CSV", "./data/medicines.csv")
        self.prediction_service = PredictionService(
            model_file=model_file, vocab_file=vocabulary_file, medicines_csv=medicines_csv
        )

        # Debug mode
        self.tweet_response = self._tweet_response if not self.debug else self._debug_response

    def ingest_tweets(self):
        # Ingest and emit raw tweets to PubSub
        raw_tweets = self.ingest()

        processed_tweets = []

        if raw_tweets is not None and len(raw_tweets) > 0:
            processed_tweets = list(map(MediTweetsService._process_tweet, raw_tweets))

        classified_tweets = self.prediction_service.predict(processed_tweets)

        # Add Tweets to db and emit classified tweets to PuSub
        self._add_tweets_to_db(classified_tweets)

        if self.should_tweet:
            # Generate responses
            self.tweet_all_responses()
        else:
            self.logger.info("Automated responses are off.")

        return classified_tweets

    @staticmethod
    def _process_tweet(tweet) -> dict:
        """
        Maps a Tweet object into a dictionary

        :Args:
            tweet (object): Tweet class from tweetpy with all the information.

        Return:
            response (dict): Dictionary with tweet values
        """

        return {
            "id": tweet.id,
            "date": tweet.created_at.__str__(),
            "raw_tweet": tweet.full_text,
            "username": tweet.user.screen_name,
            "url": TWITTER_URL.format(tweet.id),
            "hash_tags": list(map(lambda h: h["text"], tweet.entities["hashtags"])),
        }

    def _add_tweets_to_db(self, tweets):
        for result in tweets:
            if self.db_service.get_tweet_by_id(result["id"]):
                continue

            tweet = model.Tweet(
                username=result["username"],
                tweet_id=result["id"],
                tweet_ts=result["date"],
                raw_tweet=result["raw_tweet"],
                url=result["url"],
                hash_tags=result["hash_tags"],
                category=result["category"],
                components=result["components"],
                medicines=result["medicines"],
                responded=False,
            )
            self.db_service.add_tweet(tweet)

            # Emit data to event bus
            self._publish_classified_tweet(tweet)

    def _publish_classified_tweet(self, classified_tweet):
        data = classified_tweet.to_json().encode("utf-8")
        self.bus_service.publish_event(self.topic_paths["classified_tweets"], data=data)

    def tweet_all_responses(self):
        unresponded = self.db_service.get_unresponded_tweets(time_delta=1)
        self.logger.info(f"Responding to {len(unresponded)} Tweets.")

        for tweet in unresponded:
            if (BETA_TESTERS and tweet.username in BETA_TESTERS) or not BETA_TESTERS:
                self.logger.debug(
                    f"Responding to Tweet: {tweet.tweet_id}, username: {tweet.username}, \
                    medicines: {tweet.medicines}, components : {tweet.components}"
                )
                try:
                    self.tweet_response(tweet.username, tweet.tweet_id, tweet.medicines[0], tweet.components[0])
                except TweepError as e:
                    self.logger.error(e)
                tweet.responded = True
                self.db_service.flag_modified(tweet, "responded")
                # Track in event bus that tweet got responded
                self._publish_tweet_response(tweet)
                time.sleep(self.sleep_time)  # Sleep to avoid being rate limited by Twitter

            self.db_service.merge(tweet)
            self.db_service.commit()

    def _publish_tweet_response(self, tweet):
        data = json.dumps({"responded_at": int(time.time()), "tweet_id": tweet.tweet_id}).encode("utf-8")
        self.bus_service.publish_event(self.topic_paths["tweet_response"], data=data)

    def _build_text(self, username: str, medicine: str, component: str) -> str:
        component = urllib.parse.quote(component)
        permalink = f"{self.permalink}{component}"

        return f"@{username} Conseguimos la siguiente información sobre {medicine}: {permalink}"

    def _generate_response(self, username: str, medicine: str, component: str) -> str:
        """
        :Args:
        username: str, The username to whom we will reply
        medicine: str, The medicine that the user is looking for
        component: str, The link to the medicine's information

        :Return:
        response: str, a valid tweet
        """
        response = self._build_text(username, medicine, component)

        if len(response) > 280:
            self.logger.warning("Response tweet too long, generating a shorter one")
            s = Shortener(Shorteners.TINYURL)
            short_url = s.short(component)
            response = self._build_text(username, medicine, short_url)

        return response

    def _tweet_response(self, username: str, tweet_id: str, medicine: str, component: str):
        """
        This function generates the response tweet and sends it via the API

        :Args:
        keys: dict: the dictionary of API keys
        username: str: The Twitter username
        tweet_id: str: The Tweet Id if any
        component: The link with all the info
        :Returns:
        api status
        """
        api = tweepy_auth()

        response = self._generate_response(username, medicine, component)

        return api.update_status(status=response, in_reply_to_status_id=tweet_id)

    def _debug_response(self, username: str, tweet_id: str, medicine: str, component: str):
        response = self._generate_response(username, medicine, component)
        self.logger.debug(f"Generated response: {response}")
