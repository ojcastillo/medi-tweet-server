import json
import logging


class StackDriverJsonFormatter(logging.Formatter):
    def __init__(self, *args, **kwargs):
        super(StackDriverJsonFormatter, self).__init__(*args, **kwargs)

    def format(self, record):
        return json.dumps(
            {
                "severity": record.levelname,
                "timestamp": record.created,
                "message": record.getMessage(),
                "logging.googleapis.com/sourceLocation": {
                    "file": record.filename,
                    "function": record.funcName,
                    "line": record.lineno,
                },
            }
        )
