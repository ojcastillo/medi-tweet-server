# MediTweet

MediTweet is the first Tweet Bot for sourcing medicines to help with the
humanitarian crisis in Venezuela. It is part of ongoing efforts to build
effective tools to help Venezuela. See [Code4Venezuela](codeforvenezuela.org)
for more information.

For more information check [@medi_tweet_ve](https://twitter.com/medi_tweet_ve).


## Contributing

See [our contribution guidelines](./CONTRIBUTING.md).



### Install Dependencies
We recommend you install all the dependencies in a `virtualenv` or in a `conda`
environment.
```bash
conda create -n meditweet python=3.7
pip install -r requirements-test.txt
pip install -r requirements.txt
python setup.py develop
```

### Install Pre-commit Hooks
```bash
pre-commit install
```

### Code Format
Included in pre-commit hook.
```bash
black .
```

### Lint
Included in pre-commit hook.
```bash
flake8 .
```

### Test
```bash
python -m unittest
```

### Coverage
```bash
coverage run --source=example_project -m unittest discover -s tests/
coverage report -m
```

### Local Deployment and Development
```
git clone https://gitlab.com/codeforvenezuela/medi-tweets/medi-tweet-classifier.git
git clone https://gitlab.com/codeforvenezuela/medi-tweets/medi-tweet-server.git
cd medi-tweet-server
docker-compose up
```
Note: contact maintainers to get `dev` credentials.

## Maintainers
- @aesadde
- @fabix182
- @fedep3
- @jdarchitect
- @ipince
- @yessika.labrador
- @rafaelchacon
